/*
  Warnings:

  - Added the required column `price` to the `Item` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `item` ADD COLUMN `price` DOUBLE NOT NULL,
    MODIFY `detail` VARCHAR(191) NOT NULL DEFAULT '',
    MODIFY `status` BOOLEAN NOT NULL DEFAULT false;
