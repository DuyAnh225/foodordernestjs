/*
  Warnings:

  - You are about to drop the column `addressDetail` on the `customer` table. All the data in the column will be lost.
  - You are about to drop the column `addressId` on the `customer` table. All the data in the column will be lost.
  - You are about to drop the column `addressDetail` on the `foodorder` table. All the data in the column will be lost.
  - You are about to drop the column `addressId` on the `foodorder` table. All the data in the column will be lost.
  - You are about to drop the column `customerName` on the `foodorder` table. All the data in the column will be lost.
  - You are about to drop the column `customnerPhone` on the `foodorder` table. All the data in the column will be lost.
  - Added the required column `deliveryId` to the `FoodOrder` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE `customer` DROP FOREIGN KEY `Customer_addressId_fkey`;

-- DropForeignKey
ALTER TABLE `foodorder` DROP FOREIGN KEY `FoodOrder_addressId_fkey`;

-- AlterTable
ALTER TABLE `customer` DROP COLUMN `addressDetail`,
    DROP COLUMN `addressId`;

-- AlterTable
ALTER TABLE `foodorder` DROP COLUMN `addressDetail`,
    DROP COLUMN `addressId`,
    DROP COLUMN `customerName`,
    DROP COLUMN `customnerPhone`,
    ADD COLUMN `deliveryId` INTEGER NOT NULL;

-- CreateTable
CREATE TABLE `DeliveryDetail` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `customerId` INTEGER NOT NULL,
    `addressId` INTEGER NOT NULL,
    `addressDetail` VARCHAR(191) NOT NULL,
    `phone` VARCHAR(191) NOT NULL,
    `name` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `DeliveryDetail` ADD CONSTRAINT `DeliveryDetail_customerId_fkey` FOREIGN KEY (`customerId`) REFERENCES `Customer`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `DeliveryDetail` ADD CONSTRAINT `DeliveryDetail_addressId_fkey` FOREIGN KEY (`addressId`) REFERENCES `Address`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `FoodOrder` ADD CONSTRAINT `FoodOrder_deliveryId_fkey` FOREIGN KEY (`deliveryId`) REFERENCES `DeliveryDetail`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
