/*
  Warnings:

  - You are about to alter the column `roles` on the `account` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `Enum(EnumId(0))`.

*/
-- AlterTable
ALTER TABLE `account` MODIFY `roles` ENUM('CUS', 'RES') NOT NULL DEFAULT 'CUS';
