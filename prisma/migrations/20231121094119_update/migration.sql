/*
  Warnings:

  - Added the required column `addressDetail` to the `FoodOrder` table without a default value. This is not possible if the table is not empty.
  - Added the required column `addressId` to the `FoodOrder` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `foodorder` ADD COLUMN `addressDetail` VARCHAR(191) NOT NULL,
    ADD COLUMN `addressId` INTEGER NOT NULL;

-- AddForeignKey
ALTER TABLE `FoodOrder` ADD CONSTRAINT `FoodOrder_addressId_fkey` FOREIGN KEY (`addressId`) REFERENCES `Address`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
