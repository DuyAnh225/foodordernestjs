import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import axios, { AxiosRequestConfig } from 'axios';

@Injectable()
export class SaveProvinceService {
  async getProvinceData() {
    const API_KEY: string = '7f737d52-e33a-11ed-8bba-de4829400020';
    const URL_API_PROVINCE: string =
      'https://online-gateway.ghn.vn/shiip/public-api/master-data/province';

    const config: AxiosRequestConfig = {
      headers: {
        'Content-Type': 'application/json',
        token: API_KEY,
      },
    };

    try {
      const response = await axios.get(URL_API_PROVINCE, config);
      return response;
    } catch (error) {
      if (error.response && error.response.status === 401) {
        const errorMessage = error.response.data.message || 'Unauthorized';
        throw new HttpException(errorMessage, HttpStatus.UNAUTHORIZED);
      } else {
        console.error(`Error when getting province data: ${error.message}`);
        throw new HttpException(
          'Internal Server Error',
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }
}
