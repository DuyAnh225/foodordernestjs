import { Injectable } from '@nestjs/common';
import axios from 'axios';

@Injectable()
export class DistanceService {
  async calculateDistance(
    origin: string,
    destination: string,
  ): Promise<number> {
    //const apiKey = 'AIzaSyB8xBN-941Zhw1DlljWsOB1bndilSxY5sg';
    const apiKey = 'AIzaSyDaOulQACiJzBfqumbsqg_-vKha8fCnL-s';
    const url = `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${origin}&destinations=${destination}&key=${apiKey}`;

    try {
      const response = await axios.get(url);
      const data = response.data;

      // Nếu status của data không phải 'OK', in ra lỗi và ném một ngoại lệ
      if (data.status !== 'OK') {
        console.error(`Lỗi từ Google API: ${data.error_message}`);
        throw new Error('Không thể tính toán khoảng cách');
      }

      // Giả sử chúng ta chỉ quan tâm đến khoảng cách đầu tiên
      const distance = data.rows[0].elements[0].distance.value;

      return distance / 1000; // Chuyển đổi từ mét sang km
    } catch (error) {
      // In ra lỗi và ném ngoại lệ
      console.error(`Lỗi khi gọi Google API: ${error.message}`);
      throw new Error('Không thể tính toán khoảng cách');
    }
  }
}
