import { Module } from '@nestjs/common';
import { DeliverydetailService } from './deliverydetail.service';
import { DeliverydetailController } from './deliverydetail.controller';
import { PrismaService } from 'src/prisma.service';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

@Module({
  controllers: [DeliverydetailController],
  providers: [DeliverydetailService, PrismaService, JwtService, ConfigService],
})
export class DeliverydetailModule {}
