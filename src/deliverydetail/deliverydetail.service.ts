import {
  HttpException,
  HttpStatus,
  Injectable,
  Req,
  UnauthorizedException,
} from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import {
  CreateDeliveryDto,
  DeliveryDetailFilterType,
  DeliveryDetailPaginationResponseType,
  UpdateDeliveryDto,
} from './dto/deliverydetail.dto';
import { Request } from 'express';
import { DeliveryDetail } from '@prisma/client';
import { extractTokenFromHeader } from 'helpers/extractTokenFromHeader';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
@Injectable()
export class DeliverydetailService {
  constructor(
    private prismaService: PrismaService,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}
  async create(
    body: CreateDeliveryDto,
    @Req() req: Request,
  ): Promise<DeliveryDetail> {
    // step 1 : get token from header => verify & checking
    const token = await extractTokenFromHeader(req);
    if (!token) {
      throw new UnauthorizedException();
    }
    const payload = await this.jwtService.verifyAsync(token, {
      secret: this.configService.get<string>('ACCESS_TOKEN_KEY'),
    });
    const accountId = payload.id;
    const account = await this.prismaService.account.findFirst({
      where: { id: accountId },
    });
    const address = await this.prismaService.address.findFirst({
      where: { id: body.addressId },
    });
    if (!address) {
      throw new HttpException(
        { message: 'address does not exists' },
        HttpStatus.BAD_REQUEST,
      );
    }
    if (!account) {
      throw new HttpException(
        { message: 'account not found' },
        HttpStatus.BAD_REQUEST,
      );
    }
    if (payload.roles !== 'CUS') {
      throw new UnauthorizedException();
    }
    const customer = await this.prismaService.customer.findFirst({
      where: { accountId: accountId },
      select: {
        id: true,
      },
    });

    return await this.prismaService.deliveryDetail.create({
      data: {
        customerId: customer.id,
        addressId: body.addressId,
        addressDetail: body.addressDetail,
        phone: body.phone,
        name: body.name,
      },
    });
  }
  async getAll(
    filter: DeliveryDetailFilterType,
  ): Promise<DeliveryDetailPaginationResponseType> {
    const items_per_page: number = Number(filter.items_per_page) || 10;
    const page: number = filter.page || 1;
    const search: string = filter.search || '';
    const skip = page - 1 ? (page - 1) * items_per_page : 0;
    const deliveryDetail = await this.prismaService.deliveryDetail.findMany({
      take: items_per_page,
      skip: skip,
      where: {
        OR: [
          { name: { contains: search } },
          { phone: { contains: search } },
          { addressDetail: { contains: search } },
        ],
      },
      orderBy: { createdAt: 'desc' },
    });
    const total = await this.prismaService.deliveryDetail.count({
      take: items_per_page,
      skip: skip,
      where: {
        OR: [
          { name: { contains: search } },
          { phone: { contains: search } },
          { addressDetail: { contains: search } },
        ],
      },
      orderBy: { createdAt: 'desc' },
    });

    return {
      data: deliveryDetail,
      total,
      currentPage: page,
      itemsPerPage: items_per_page,
    };
  }
  async getDetail(id: number): Promise<DeliveryDetail> {
    const deliveryDetail = await this.prismaService.deliveryDetail.findFirst({
      where: { id },
    });
    if (!deliveryDetail) {
      throw new HttpException(
        { message: 'delivery detail not found' },
        HttpStatus.NOT_FOUND,
      );
    }
    return deliveryDetail;
  }
  async update(id: number, data: UpdateDeliveryDto): Promise<DeliveryDetail> {
    const deliveryDetail = await this.prismaService.deliveryDetail.findFirst({
      where: {
        id,
      },
    });
    if (!deliveryDetail) {
      throw new HttpException(
        { message: 'delivery detail not found' },
        HttpStatus.BAD_REQUEST,
      );
    }
    return await this.prismaService.deliveryDetail.update({
      where: { id },
      data,
    });
  }

  async delete(id: number): Promise<DeliveryDetail> {
    const deliveryDetail = await this.prismaService.deliveryDetail.findFirst({
      where: {
        id,
      },
    });
    if (!deliveryDetail) {
      throw new HttpException(
        { message: 'delivery detail not found' },
        HttpStatus.NOT_FOUND,
      );
    }
    return await this.prismaService.deliveryDetail.delete({
      where: { id },
    });
  }
}
