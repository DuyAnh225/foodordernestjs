import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  Req,
} from '@nestjs/common';
import { DeliverydetailService } from './deliverydetail.service';
import {
  CreateDeliveryDto,
  DeliveryDetailFilterType,
  DeliveryDetailPaginationResponseType,
  UpdateDeliveryDto,
} from './dto/deliverydetail.dto';
import { DeliveryDetail } from '@prisma/client';
import { Request } from 'express';
import { Roles } from 'src/auth/decorator/role.decorator';
@Controller('deliverydetail')
export class DeliverydetailController {
  constructor(private readonly deliverydetailService: DeliverydetailService) {}
  // create POST
  @Post()
  @Roles('CUS')
  create(
    @Body() body: CreateDeliveryDto,
    @Req() req: Request,
  ): Promise<DeliveryDetail> {
    console.log('run api create delivery detail');
    return this.deliverydetailService.create(body, req);
  }

  //GetALL
  @Get()
  @Roles('CUS')
  getAll(
    @Query() param: DeliveryDetailFilterType,
  ): Promise<DeliveryDetailPaginationResponseType> {
    console.log('run api get all delivery detail');
    return this.deliverydetailService.getAll(param);
  }
  //get detail
  @Get(':id')
  @Roles('CUS', 'AD')
  getDetail(@Param('id', ParseIntPipe) id: number): Promise<DeliveryDetail> {
    console.log('run api get detail delivery ', id);
    return this.deliverydetailService.getDetail(id);
  }
  //Update
  @Put(':id')
  @Roles('CUS', 'AD')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() data: UpdateDeliveryDto,
  ): Promise<DeliveryDetail> {
    console.log('run api update delivery detail');
    return this.deliverydetailService.update(id, data);
  }
  //delete
  @Delete(':id')
  @Roles('CUS', 'AD')
  delete(@Param('id', ParseIntPipe) id: number): Promise<DeliveryDetail> {
    console.log('run api delete delivery detail ', id);
    return this.deliverydetailService.delete(id);
  }
}
