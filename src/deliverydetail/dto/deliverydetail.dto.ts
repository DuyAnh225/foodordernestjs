import { DeliveryDetail } from '@prisma/client';
import { IsNotEmpty, IsOptional, Matches } from 'class-validator';

export class CreateDeliveryDto {
  @IsNotEmpty()
  addressId: number;
  @IsNotEmpty()
  addressDetail: string;
  @Matches(/(84|0[3|5|7|8|9])+([0-9]{8})\b/g)
  phone: string;
  @IsNotEmpty()
  name: string;
}

export class UpdateDeliveryDto {
  @IsOptional()
  addressId?: number;
  @IsOptional()
  addressDetail?: string;
  @IsOptional()
  @Matches(/(84|0[3|5|7|8|9])+([0-9]{8})\b/g)
  phone?: string;
  @IsOptional()
  name?: string;
}
export interface DeliveryDetailFilterType {
  items_per_page?: number;
  page?: number;
  search?: string;
}
export interface DeliveryDetailPaginationResponseType {
  data: DeliveryDetail[];
  total: number;
  currentPage: number;
  itemsPerPage: number;
}
