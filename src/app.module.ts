import { Module, ValidationPipe } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaService } from './prisma.service';
import { AuthModule } from './auth/auth.module';
import { CustomerModule } from './customer/customer.module';
import { APP_GUARD, APP_PIPE } from '@nestjs/core';
import { AuthGuard } from './auth/auth.guard';
import { RoleGuard } from './auth/roles.guard';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { RestaurantModule } from './restaurant/restaurant.module';
import { FoodorderModule } from './foodorder/foodorder.module';
import { DeliverydetailModule } from './deliverydetail/deliverydetail.module';
import { ItemModule } from './item/item.module';

@Module({
  imports: [AuthModule, CustomerModule, RestaurantModule, FoodorderModule, DeliverydetailModule, ItemModule],
  controllers: [AppController],
  providers: [
    AppService,
    PrismaService,
    JwtService,
    ConfigService,
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
    {
      provide: APP_GUARD,
      useClass: RoleGuard,
    },
  ],
})
export class AppModule {}
