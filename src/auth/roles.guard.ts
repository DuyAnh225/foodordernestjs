import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    console.log('vào role guard');
    const requireRoles = this.reflector.getAllAndOverride<string[]>('roles', [
      context.getHandler(),
      context.getClass(),
    ]);
    if (!requireRoles) {
      return true;
    }
    console.log('vao role ne');
    const { account } = context.switchToHttp().getRequest();
    console.log('account ne:' + account);
    return requireRoles.some((role) => account.roles.split(',').includes(role));
  }
}
