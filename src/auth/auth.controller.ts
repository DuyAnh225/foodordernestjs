import { Controller, Post, Body, Req } from '@nestjs/common';
import { LoginDto, RegisterDto } from './dto/auth.dto';
import { Account } from '@prisma/client';
import { AuthService } from './auth.service';
import { Public } from './decorator/public.decorator';
import { Request } from 'express';
import { UseGuards } from '@nestjs/common/decorators';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}
  @Post('register')
  @Public()
  register(@Body() body: RegisterDto): Promise<Account> {
    return this.authService.register(body);
  }
  @Post('login')
  @Public()
  login(@Body() body: LoginDto): Promise<Account> {
    return this.authService.login(body);
  }
  @Post('logout')
  @Public()
  logout(@Req() req: Request): Promise<Account> {
    return this.authService.logout(req);
  }
  // @Post('refresh-token')
  // @Public()
  // refreshToken(@Body() { refreshToken }): Promise<any> {
  //   console.log('run api refresh token ');
  //   return this.authService.refreshToken(refreshToken);
  // }
}
