import {
  Injectable,
  HttpException,
  HttpStatus,
  Req,
  UnauthorizedException,
} from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { LoginDto, RegisterDto } from './dto/auth.dto';
import { Account, Customer } from '@prisma/client';
import { compare, hash } from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { ROLES } from 'helpers/roles';
import { extractTokenFromHeader } from 'helpers/extractTokenFromHeader';
import { Request } from 'express';

@Injectable()
export class AuthService {
  constructor(
    private prismaService: PrismaService,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}
  register = async (accountData: RegisterDto): Promise<any> => {
    //step 1: checking exist email
    const account = await this.prismaService.account.findFirst({
      where: {
        email: accountData.email,
      },
    });
    if (!ROLES.includes(accountData.roles)) {
      throw new HttpException(
        { message: 'No permission exist' },
        HttpStatus.BAD_REQUEST,
      );
    }
    if (account) {
      throw new HttpException(
        {
          message: 'this email has been used',
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    //step 2 : hash password
    const hashPassword: any = await hash(accountData.password, 10);
    const res = await this.prismaService.account.create({
      data: {
        ...accountData,
        password: hashPassword,
      },
      select: {
        email: true,
        roles: true,
        createdAt: true,
      },
    });
    return res;
  };
  login = async (accountData: LoginDto): Promise<any> => {
    //step 1 : checking exists email
    const account = await this.prismaService.account.findFirst({
      where: { email: accountData.email },
    });
    if (!account) {
      throw new HttpException(
        {
          message: 'account not found',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    // step 2 : checking password
    const verify = await compare(accountData.password, account.password);
    if (!verify) {
      throw new HttpException(
        { message: 'password is not correct' },
        HttpStatus.BAD_REQUEST,
      );
    }
    //step 3: generate accessToken and refresh token
    const payload = {
      id: account.id,
      email: accountData.email,
      roles: account.roles,
    };
    return this.generateToken(payload);
  };
  private async generateToken(payload: {
    id: number;
    email: string;
    roles: string;
  }) {
    const accessToken = await this.jwtService.signAsync(payload, {
      secret: this.configService.get<string>('ACCESS_TOKEN_KEY'),
      expiresIn: this.configService.get<string>('EXPIRESIN_ACCESS_TOKEN'),
    });
    const refreshToken = await this.jwtService.signAsync(payload, {
      secret: this.configService.get<string>('REFRESH_TOKEN_KEY'),
      expiresIn: this.configService.get<string>('EXPIRESIN_REFRESH_TOKEN'),
    });
    await this.prismaService.account.update({
      where: { email: payload.email },
      data: {
        refreshToken,
      },
    });
    return { accessToken, refreshToken };
  }
  async logout(@Req() req: Request): Promise<Account> {
    const token = await extractTokenFromHeader(req);
    if (!token) {
      throw new UnauthorizedException();
    }
    const payload = await this.jwtService.verifyAsync(token, {
      secret: this.configService.get<string>('ACCESS_TOKEN_KEY'),
    });
    const account = await this.prismaService.account.findUnique({
      where: {
        id: payload.id,
      },
    });
    if (!account) {
      throw new HttpException(
        { message: 'account not found' },
        HttpStatus.NOT_FOUND,
      );
    }

    return await this.prismaService.account.update({
      where: { id: payload.id },
      data: {
        refreshToken: '',
      },
    });
  }
  // refresh Token
  // async refreshToken(refreshToken: string): Promise<any> {
  //   const payload = await this.jwtService.verifyAsync(refreshToken, {
  //     secret: this.configService.get<string>('REFRESH_TOKEN_KEY'),
  //   });
  //   if (!payload) {
  //     throw new HttpException(
  //       { message: 'refresh token is not valid' },
  //       HttpStatus.FORBIDDEN,
  //     );
  //   }
  //   const checkExistToken = await this.prismaService.account.findFirst({
  //     where: {
  //       AND: [{ id: payload.id }, { email: payload.email }],
  //     },
  //   });
  //   if (!checkExistToken) {
  //     throw new HttpException(
  //       { message: 'refresh token is not valid' },
  //       HttpStatus.BAD_REQUEST,
  //     );
  //   }

  //   return this.generateToken(payload);
  // }
}
