import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { PrismaService } from 'src/prisma.service';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import { Observable } from 'rxjs';
import { extractTokenFromHeader } from 'helpers/extractTokenFromHeader';
import { Account } from '@prisma/client';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private jwtService: JwtService,
    private configService: ConfigService,
    private prismaService: PrismaService,
    private reflector: Reflector,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const isPublic = this.reflector.getAllAndOverride<string[]>('isPublic', [
      context.getHandler(),
      context.getClass(),
    ]);
    if (isPublic) {
      return true;
    }
    console.log('is public ' + isPublic);
    const request = context.switchToHttp().getRequest();
    const token = await extractTokenFromHeader(request);
    if (!token) {
      throw new UnauthorizedException();
    }
    try {
      const payload = await this.jwtService.verifyAsync(token, {
        secret: this.configService.get<string>('ACCESS_TOKEN_KEY'),
      });
      request['account_data'] = payload;
      const account = await this.prismaService.account.findFirst({
        where: {
          id: payload.id,
        },
      });
      request['account'] = account;
    } catch {
      throw new UnauthorizedException();
    }
    return true;
  }
  async getToken(context: ExecutionContext): Promise<string> {
    const request = context.switchToHttp().getRequest();
    const token = await extractTokenFromHeader(request);
    if (!token) {
      throw new UnauthorizedException();
    }

    return token;
  }
}
