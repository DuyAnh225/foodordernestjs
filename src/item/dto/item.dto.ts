import { Item } from '@prisma/client';
import { IsNotEmpty, IsNumber, IsOptional, Min, min } from 'class-validator';

export class CreateItemDto {
  @IsNotEmpty()
  name: string;
  @IsOptional()
  detail?: string;
  @IsOptional()
  status?: boolean;
  @IsOptional()
  image?: string;
  @IsNotEmpty()
  categoryId: number;
  @IsNotEmpty()
  @IsNumber()
  @Min(1000)
  price: number;
}
export class UpdateItemDto {
  @IsOptional()
  name?: string;
  @IsOptional()
  detail?: string;
  @IsOptional()
  status?: boolean;
  @IsOptional()
  image?: string;
  @IsOptional()
  categoryId?: number;
}
export interface ItemFilterType {
  items_per_page?: number;
  page?: number;
  search?: string;
  categoryId?: number;
  restaurantId?: number;
}
export interface ItemPaginationResponseType {
  data: Item[];
  total: number;
  currentPage: number;
  itemsPerPage: number;
}
