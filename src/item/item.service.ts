import {
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import {
  CreateItemDto,
  ItemFilterType,
  ItemPaginationResponseType,
  UpdateItemDto,
} from './dto/item.dto';
import { Item } from '@prisma/client';
import { Request } from 'express';
import { extractTokenFromHeader } from 'helpers/extractTokenFromHeader';
import { ConfigService } from '@nestjs/config';
import { PrismaService } from 'src/prisma.service';
import { JwtService } from '@nestjs/jwt';
@Injectable()
export class ItemService {
  constructor(
    private configService: ConfigService,
    private prismaService: PrismaService,
    private jwtService: JwtService,
  ) {}
  async create(body: CreateItemDto, req: Request): Promise<Item> {
    // step 1 : get token from header ==> verify & checking
    const token = await extractTokenFromHeader(req);
    if (!token) {
      throw new UnauthorizedException();
    }
    const payload = await this.jwtService.verifyAsync(token, {
      secret: this.configService.get<string>('ACCESS_TOKEN_KEY'),
    });
    if (!payload) {
      throw new HttpException(
        { message: 'token is not valid' },
        HttpStatus.BAD_REQUEST,
      );
    }
    const accountId = payload.id;
    const categoryId = body.categoryId;
    const category = await this.prismaService.category.findFirst({
      where: { id: categoryId },
    });
    if (!category) {
      throw new HttpException(
        { message: 'category not found' },
        HttpStatus.NOT_FOUND,
      );
    }
    const account = await this.prismaService.account.findFirst({
      where: { id: accountId },
    });
    if (!account) {
      throw new HttpException(
        { message: 'account not found' },
        HttpStatus.NOT_FOUND,
      );
    }
    if (payload.roles != 'RES') {
      throw new HttpException(
        { message: 'account does not have permission' },
        HttpStatus.FORBIDDEN,
      );
    }
    const restaurant = await this.prismaService.restaurant.findFirst({
      where: { accountId: accountId },
    });
    if (!restaurant) {
      throw new HttpException(
        { message: 'restaurant not found' },
        HttpStatus.NOT_FOUND,
      );
    }

    return await this.prismaService.item.create({
      data: {
        name: body.name,
        detail: body.detail,
        image: body.image,
        status: body.status,
        categoryId: categoryId,
        restaurantId: restaurant.id,
        price: body.price,
      },
    });
  }
  async getAll(filter: ItemFilterType): Promise<ItemPaginationResponseType> {
    const items_per_page: number = Number(filter.items_per_page) || 10;
    const page: number = Number(filter.page) || 1;
    const search: string = filter.search || '';
    const skip = page - 1 ? (page - 1) * items_per_page : 0;
    const categoryId = Number(filter.categoryId);
    const restaurantId = Number(filter.restaurantId);
    const whereConditions: any = {
      OR: [
        { name: { contains: search } },
        { detail: { contains: search } },
        { image: { contains: search } },
      ],
    };
    if (!isNaN(categoryId)) {
      const category = await this.prismaService.category.findFirst({
        where: { id: categoryId },
      });
      if (!category) {
        throw new HttpException(
          { message: 'category not found' },
          HttpStatus.NOT_FOUND,
        );
      }
      whereConditions.categoryId = categoryId;
    }
    if (!isNaN(restaurantId)) {
      const restaurant = await this.prismaService.restaurant.findFirst({
        where: { id: restaurantId },
      });
      if (!restaurant) {
        throw new HttpException(
          { message: 'restaurant not found' },
          HttpStatus.NOT_FOUND,
        );
      }
      whereConditions.restaurantId = restaurantId;
    }

    const items = await this.prismaService.item.findMany({
      take: items_per_page,
      skip: skip,
      where: whereConditions,
      orderBy: { createdAt: 'desc' },
    });
    const total = await this.prismaService.item.count({
      take: items_per_page,
      skip: skip,
      where: whereConditions,
      orderBy: { createdAt: 'desc' },
    });
    return {
      data: items,
      total,
      currentPage: page,
      itemsPerPage: items_per_page,
    };
  }
  // get detail
  async getDetail(id: number): Promise<Item> {
    const item = await this.prismaService.item.findFirst({
      where: { id },
    });
    if (!item) {
      throw new HttpException(
        { message: 'item not found' },
        HttpStatus.NOT_FOUND,
      );
    }
    return item;
  }
  // update item
  async update(id: number, data: UpdateItemDto): Promise<Item> {
    const item = await this.prismaService.item.findFirst({
      where: { id },
    });
    if (!item) {
      throw new HttpException(
        { message: 'item not found' },
        HttpStatus.BAD_REQUEST,
      );
    }
    return this.prismaService.item.update({
      where: { id },
      data,
    });
  }
  //delete
  async delete(id: number): Promise<Item> {
    const item = await this.prismaService.item.findFirst({
      where: { id },
    });
    if (!item) {
      throw new HttpException(
        { message: 'item not found' },
        HttpStatus.NOT_FOUND,
      );
    }
    return await this.prismaService.item.delete({
      where: { id },
    });
  }
}
