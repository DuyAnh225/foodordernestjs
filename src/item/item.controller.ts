import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  Req,
} from '@nestjs/common';
import { ItemService } from './item.service';
import { Roles } from 'src/auth/decorator/role.decorator';
import {
  CreateItemDto,
  ItemFilterType,
  ItemPaginationResponseType,
  UpdateItemDto,
} from './dto/item.dto';
import { Item } from '@prisma/client';
import { Request } from 'express';
@Controller('item')
export class ItemController {
  constructor(private readonly itemService: ItemService) {}

  //create item
  @Post()
  @Roles('RES', 'AD')
  create(@Body() body: CreateItemDto, @Req() req: Request): Promise<Item> {
    return this.itemService.create(body, req);
  }

  //getAll item + search  + pagination
  @Get()
  @Roles('RES', 'AD', 'CUS')
  getAll(@Query() param: ItemFilterType): Promise<ItemPaginationResponseType> {
    console.log('run api get all item');
    return this.itemService.getAll(param);
  }
  //get detail
  @Get(':id')
  @Roles('RES', 'AD', 'CUS')
  getDetail(@Param('id', ParseIntPipe) id: number): Promise<Item> {
    console.log('run api get detail item ', id);
    return this.itemService.getDetail(id);
  }
  //update item
  @Put(':id')
  @Roles('RES', 'AD')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() body: UpdateItemDto,
  ): Promise<Item> {
    console.log('run api update item');
    return this.itemService.update(id, body);
  }
  //delete item
  @Delete(':id')
  @Roles('RES', 'AD')
  delete(@Param('id', ParseIntPipe) id: number): Promise<Item> {
    console.log('run api delete item', id);
    return this.itemService.delete(id);
  }
}
