import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  Req,
} from '@nestjs/common';
import { CustomerService } from './customer.service';
import {
  CreateCustomerDto,
  CustomerFilterType,
  CustomerPaginationResponseType,
  UpdateCustomerDto,
} from './dto/customer.dto';
import { Account, Customer } from '@prisma/client';
import { Roles } from 'src/auth/decorator/role.decorator';
import { Request } from 'express';

@Controller('customer')
export class CustomerController {
  constructor(private readonly customerService: CustomerService) {}
  @Post()
  @Roles('CUS')
  create(
    @Body() body: CreateCustomerDto,
    @Req() req: Request,
  ): Promise<Customer> {
    console.log('create account api ' + body);
    return this.customerService.create(body, req);
  }
  @Roles('CUS', 'AD')
  @Put(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() data: UpdateCustomerDto,
  ) {
    console.log('run api update customer ' + id);
    return this.customerService.update(id, data);
  }

  @Get()
  @Roles('CUS', 'AD')
  getAll(
    @Query() params: CustomerFilterType,
  ): Promise<CustomerPaginationResponseType> {
    console.log('run api get all customer');
    return this.customerService.getAll(params);
  }

  @Get(':id')
  @Roles('RES', 'AD')
  getDetail(@Param('id', ParseIntPipe) id: number): Promise<Customer> {
    console.log('run api get detail customer ' + id);
    return this.customerService.getDetail(id);
  }

  @Delete(':id')
  @Roles('AD')
  delete(@Param('id', ParseIntPipe) id: number): Promise<Customer> {
    console.log('run api delete customer ', id);
    return this.customerService.delete(id);
  }
}
