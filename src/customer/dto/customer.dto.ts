import { Account, Customer } from '@prisma/client';
import { IsNotEmpty, IsOptional, Matches } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  @Matches(/(84|0[3|5|7|8|9])+([0-9]{8})\b/g)
  phone: string;
}
export class UpdateCustomerDto {
  @IsOptional()
  name?: string;
  @IsOptional()
  @Matches(/(84|0[3|5|7|8|9])+([0-9]{8})\b/g)
  phone?: string;
}

export interface CustomerFilterType {
  items_per_page?: number;
  page?: number;
  search?: string;
}
export interface CustomerPaginationResponseType {
  data: Customer[];
  total: number;
  currentPage: number;
  itemsPerPage: number;
}
