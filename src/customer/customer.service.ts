import {
  Injectable,
  CanActivate,
  ExecutionContext,
  UnauthorizedException,
  Req,
  ParseIntPipe,
} from '@nestjs/common';
import {
  CreateCustomerDto,
  CustomerFilterType,
  CustomerPaginationResponseType,
  UpdateCustomerDto,
} from './dto/customer.dto';
import { Customer } from '@prisma/client';
import { PrismaService } from 'src/prisma.service';
import { Request } from 'express';
import { JwtService } from '@nestjs/jwt';
import { extractTokenFromHeader } from 'helpers/extractTokenFromHeader';
import { ConfigService } from '@nestjs/config';
import { HttpException, HttpStatus } from '@nestjs/common';

@Injectable()
export class CustomerService {
  constructor(
    private prismaService: PrismaService,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}

  async create(
    body: CreateCustomerDto,
    @Req() req: Request,
  ): Promise<Customer> {
    const token = await extractTokenFromHeader(req);
    console.log(token);
    if (!token) {
      throw new UnauthorizedException();
    }
    const payload = await this.jwtService.verifyAsync(token, {
      secret: this.configService.get<string>('ACCESS_TOKEN_KEY'),
    });

    if (payload.roles === 'CUS') {
      const accountId: number = payload.id;
      const customer = await this.prismaService.customer.create({
        data: {
          name: body.name,
          accountId: accountId,
          phone: body.phone,
        },
      });
      return customer;
    } else {
      throw new UnauthorizedException();
    }
  }
  async getAll(
    filter: CustomerFilterType,
  ): Promise<CustomerPaginationResponseType> {
    const items_per_page: number = Number(filter.items_per_page) || 10;
    const page: number = filter.page || 1;
    const search: string = filter.search || '';
    const skip = page - 1 ? (page - 1) * items_per_page : 0;
    const customers = await this.prismaService.customer.findMany({
      take: items_per_page,
      skip: skip,
      where: {
        OR: [{ name: { contains: search } }, { phone: { contains: search } }],
      },
      orderBy: { createdAt: 'desc' },
    });
    const total = await this.prismaService.customer.count({
      take: items_per_page,
      skip: skip,
      where: {
        OR: [{ name: { contains: search } }, { phone: { contains: search } }],
      },
      orderBy: { createdAt: 'desc' },
    });

    return {
      data: customers,
      total,
      currentPage: page,
      itemsPerPage: items_per_page,
    };
  }
  async getDetail(id: number): Promise<Customer> {
    const customer = await this.prismaService.customer.findFirst({
      where: {
        id,
      },
    });
    if (!customer) {
      throw new HttpException(
        { message: 'customer not found' },
        HttpStatus.NOT_FOUND,
      );
    }
    return customer;
  }
  async update(id: number, data: UpdateCustomerDto): Promise<Customer> {
    const customer = await this.prismaService.customer.findFirst({
      where: {
        id,
      },
    });
    if (!customer) {
      throw new HttpException(
        { message: 'customer not found' },
        HttpStatus.NOT_FOUND,
      );
    }
    return await this.prismaService.customer.update({
      where: { id },
      data,
    });
  }
  async delete(id: number): Promise<Customer> {
    const customer = await this.prismaService.customer.findFirst({
      where: {
        id,
      },
    });

    if (!customer) {
      throw new HttpException(
        { message: 'customer not found' },
        HttpStatus.NOT_FOUND,
      );
    }
    const accountId = customer.accountId;
    const customerDel = await this.prismaService.customer.delete({
      where: {
        id,
      },
    });
    const accountDel = await this.prismaService.account.delete({
      where: { id: accountId },
    });

    return customerDel;
  }
}
