import {
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { PrismaService } from 'src/prisma.service';
import {
  CreateFoodOrderDto,
  FoodOrderFilterType,
  FoodOrderPaginationResponseType,
  ORDER_STATUS,
  UpdateFoodOrderDto,
} from './dto/foodOrder.dto';
import { FoodOrder } from '@prisma/client';
import { Request } from 'express';
import { extractTokenFromHeader } from 'helpers/extractTokenFromHeader';
import axios from 'axios';

import { SaveProvinceService } from 'helpers/province.api';
import { DistanceService } from 'helpers/distanceCalculation.service';
import { Res } from '@nestjs/common';

@Injectable()
export class FoodorderService {
  constructor(
    private jwtService: JwtService,
    private configService: ConfigService,
    private prismaService: PrismaService,
    private distanceService: DistanceService,
    private saveProvinceService: SaveProvinceService,
  ) {}
  //Tính toán khoảng cách bằng api bên thứ 3( ghn, gg map api)
  // async test(): Promise<any> {
  //   return this.saveProvinceService.getProvinceData();
  // }
  // async test2(): Promise<any> {
  //   return this.distanceService.calculateDistance(
  //     'đông á , đông hưng, thái bình',
  //     'ngõ 80 xuân phương, phương canh, nam từ liêm, hà nội',
  //   );
  // }

  async create(body: CreateFoodOrderDto, req: Request): Promise<any> {
    //step 1 : get token from header == verify & checking
    const token = await extractTokenFromHeader(req);
    if (!token) {
      throw new UnauthorizedException();
    }
    const payload = await this.jwtService.verifyAsync(token, {
      secret: this.configService.get<string>('ACCESS_TOKEN_KEY'),
    });
    if (payload.roles !== 'CUS') {
      throw new UnauthorizedException();
    }
    const accountId = payload.id;
    const account = await this.prismaService.account.findFirst({
      where: { id: accountId },
    });
    if (!account) {
      throw new HttpException(
        { message: 'account not found' },
        HttpStatus.BAD_REQUEST,
      );
    }
    const deliveryDetailId = body.deliveryId;
    const deliveryDetail = await this.prismaService.deliveryDetail.findFirst({
      where: { id: deliveryDetailId },
    });
    if (!deliveryDetail) {
      throw new HttpException(
        { message: 'delivery detail not found' },
        HttpStatus.BAD_REQUEST,
      );
    }
    const restaurantId = body.restaurantId;
    const restaurant = await this.prismaService.restaurant.findUnique({
      where: { id: restaurantId },
    });
    if (!restaurant) {
      throw new HttpException(
        { message: 'restaurant detail not found' },
        HttpStatus.BAD_REQUEST,
      );
    }
    const customer = await this.prismaService.customer.findFirst({
      where: { accountId: accountId },
      select: { id: true },
    });
    let deliveryFee: number = 0;
    // step 2: calculate delivery fee
    const distance: number = body.distance;
    if (distance <= 0) {
      throw new HttpException(
        {
          message: 'distance error',
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    if (distance < 2) {
      deliveryFee = 15000;
    } else if (distance < 5) {
      deliveryFee = 3000;
    } else {
      throw new HttpException(
        { message: 'Delivery distance is too far' },
        HttpStatus.BAD_REQUEST,
      );
    }

    return await this.prismaService.foodOrder.create({
      data: {
        restaurantId: restaurantId,
        orderDatetime: body.orderDatetime,
        note: body.note,
        deliveryId: body.deliveryId,
        deliveryFee: deliveryFee,
        customerId: customer.id,
      },
    });
  }
  async getAll(
    filter: FoodOrderFilterType,
  ): Promise<FoodOrderPaginationResponseType> {
    const items_per_page: number = Number(filter.items_per_page) || 10;
    const page: number = filter.page || 1;
    const search: string = filter.search || '';
    const skip = page - 1 ? (page - 1) * items_per_page : 0;
    const customerId = Number(filter.customerId);
    const restaurantId = Number(filter.restaurantId);
    const orderStatus = filter.orderStatus;
    console.log(orderStatus);

    const whereConditions: any = {
      OR: [{ note: { contains: search } }],
    };
    if (orderStatus) {
      const orderStatusUpper = orderStatus.toUpperCase();
      if (ORDER_STATUS.includes(orderStatusUpper)) {
        whereConditions.orderStatus = orderStatus;
      } else {
        throw new HttpException(
          { message: 'order status fail' },
          HttpStatus.BAD_REQUEST,
        );
      }
    }
    if (!isNaN(customerId)) {
      whereConditions.customerId = customerId;
    }
    if (!isNaN(restaurantId)) {
      whereConditions.restaurantId = restaurantId;
    }

    const foodOrders = await this.prismaService.foodOrder.findMany({
      take: items_per_page,
      skip: skip,
      where: whereConditions,
      orderBy: { createdAt: 'desc' },
    });
    const total = await this.prismaService.foodOrder.count({
      take: items_per_page,
      skip: skip,
      where: whereConditions,
      orderBy: { createdAt: 'desc' },
    });
    return {
      data: foodOrders,
      total,
      currentPage: page,
      itemsPerPage: items_per_page,
    };
  }
  async getDetail(id: number): Promise<FoodOrder> {
    const foodOrder = await this.prismaService.foodOrder.findFirst({
      where: { id },
    });
    if (!foodOrder) {
      throw new HttpException(
        { message: 'food order not found' },
        HttpStatus.NOT_FOUND,
      );
    }
    return foodOrder;
  }

  async update(
    id: number,
    data: UpdateFoodOrderDto,
    req: Request,
  ): Promise<FoodOrder> {
    const foodOrder = await this.prismaService.foodOrder.findFirst({
      where: { id },
    });
    if (!foodOrder) {
      throw new HttpException(
        { message: 'food order not found' },
        HttpStatus.NOT_FOUND,
      );
    }

    if (foodOrder.orderStatus == 'COMPLETE') {
      throw new HttpException(
        { message: 'do not update food order ' },
        HttpStatus.BAD_REQUEST,
      );
    }
    //get roles  current account
    const token = await extractTokenFromHeader(req);
    if (!token) {
      throw new UnauthorizedException();
    }
    const payload = await this.jwtService.verifyAsync(token, {
      secret: this.configService.get<string>('ACCESS_TOKEN_KEY'),
    });
    if (payload.roles == 'RES' && (data.note || data.deliveryId)) {
      throw new UnauthorizedException();
    }
    return this.prismaService.foodOrder.update({
      where: { id },
      data,
    });
  }

  async delete(id: number): Promise<FoodOrder> {
    const foodOrder = await this.prismaService.foodOrder.findFirst({
      where: { id },
    });
    if (!foodOrder) {
      throw new HttpException(
        { message: 'food order not fount' },
        HttpStatus.NOT_FOUND,
      );
    }
    return await this.prismaService.foodOrder.delete({
      where: { id },
    });
  }
}
