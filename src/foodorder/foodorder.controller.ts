import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  Req,
  Res,
} from '@nestjs/common';
import { FoodorderService } from './foodorder.service';
import { Roles } from 'src/auth/decorator/role.decorator';
import {
  CreateFoodOrderDto,
  FoodOrderFilterType,
  FoodOrderPaginationResponseType,
  UpdateFoodOrderDto,
} from './dto/foodOrder.dto';
import { FoodOrder } from '@prisma/client';
import { Request, Response } from 'express';

import { Public } from 'src/auth/decorator/public.decorator';
import { SaveProvinceService } from 'helpers/province.api';

@Controller('foodorder')
export class FoodorderController {
  constructor(private readonly foodorderService: FoodorderService) {}

  // @Get()
  // @Public()
  // getProvince(): Promise<any> {
  //   console.log('run api get province');
  //   return this.foodorderService.test2();
  // }
  // @Post()
  // @Public()
  // getProvince2(@Res() res: Response): Promise<any> {
  //   console.log('run api get province');
  //   return this.foodorderService.test();
  // }

  //create food order
  @Post()
  @Roles('CUS')
  create(@Body() body: CreateFoodOrderDto, @Req() req: Request): Promise<any> {
    console.log('run api create foodorder');
    return this.foodorderService.create(body, req);
  }
  //get all food order + search & pagination
  @Get()
  @Roles('CUS', 'AD', 'RES')
  getAll(
    @Query() param: FoodOrderFilterType,
  ): Promise<FoodOrderPaginationResponseType> {
    console.log('run api get all food order');
    return this.foodorderService.getAll(param);
  }
  // get detail
  @Get(':id')
  @Roles('CUS', 'AD', 'RES')
  getDetail(@Param('id', ParseIntPipe) id: number): Promise<FoodOrder> {
    console.log('run api get detail');
    return this.foodorderService.getDetail(id);
  }

  //update food order
  @Put(':id')
  @Roles('CUS', 'AD', 'RES')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() body: UpdateFoodOrderDto,
    @Req() req: Request,
  ): Promise<FoodOrder> {
    console.log('run api update food order');
    return this.foodorderService.update(id, body, req);
  }

  // delete food order
  @Delete(':id')
  @Roles('AD')
  delete(@Param('id', ParseIntPipe) id: number): Promise<FoodOrder> {
    console.log('run api delete foodorder');
    return this.foodorderService.delete(id);
  }
}
