import { IsNotEmpty, IsNumber, IsOptional, Min } from 'class-validator';

import { FoodOrder } from '@prisma/client';

export class CreateFoodOrderDto {
  @IsNotEmpty()
  @IsNumber()
  restaurantId: number;
  @IsNotEmpty()
  orderDatetime: Date;
  @IsOptional()
  note?: string;
  @IsNotEmpty()
  @IsNumber()
  deliveryId: number;
  @IsNumber()
  @IsNotEmpty()
  @Min(0)
  distance: number;
}

export class UpdateFoodOrderDto {
  @IsOptional()
  note?: string;
  @IsOptional()
  @IsNumber()
  deliveryId?: number;
  @IsOptional()
  orderStatus?: string;
}

export interface FoodOrderFilterType {
  items_per_page?: number;
  page?: number;
  search?: string;
  orderStatus?: string;
  customerId: number;
  restaurantId: number;
}
export interface FoodOrderPaginationResponseType {
  data: FoodOrder[];
  total: number;
  currentPage: number;
  itemsPerPage: number;
}

export const ORDER_STATUS = ['WAIT', 'ORDERED', 'COMPLETE'];
