import { Module } from '@nestjs/common';
import { FoodorderService } from './foodorder.service';
import { FoodorderController } from './foodorder.controller';
import { PrismaService } from 'src/prisma.service';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

import { SaveProvinceService } from 'helpers/province.api';
import { DistanceService } from 'helpers/distanceCalculation.service';

@Module({
  controllers: [FoodorderController],
  providers: [
    FoodorderService,
    PrismaService,
    JwtService,
    ConfigService,
    DistanceService,
    SaveProvinceService,
  ],
})
export class FoodorderModule {}
