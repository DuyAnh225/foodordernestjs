import { Restaurant } from '@prisma/client';
import { IsNotEmpty, IsOptional, Matches } from 'class-validator';

export class CreateRestaurantDto {
  @IsNotEmpty()
  name: string;
  @IsOptional()
  detail?: string;
  @IsNotEmpty()
  addressId: number;
  @IsOptional()
  status: boolean;
  @IsOptional()
  addressDetail?: string;
  @IsNotEmpty()
  @Matches(/(84|0[3|5|7|8|9])+([0-9]{8})\b/g)
  phone: string;
}
export class UpdateRestaurantDto {
  @IsOptional()
  name?: string;
  @IsOptional()
  detail?: string;
  @IsOptional()
  @Matches(/(84|0[3|5|7|8|9])+([0-9]{8})\b/g)
  phone?: string;
  @IsOptional()
  status?: boolean;
  @IsOptional()
  addressDetail?: string;
  @IsOptional()
  addressId?: number;
}

export interface RestaurantFilterType {
  items_per_page?: number;
  page?: number;
  search?: string;
}
export interface RestaurantPaginationResponseType {
  data: Restaurant[];
  total: number;
  currentPage: number;
  itemsPerPage: number;
}
