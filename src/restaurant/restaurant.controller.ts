import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  ParseIntPipe,
  Post,
  Put,
  Req,
} from '@nestjs/common';
import { RestaurantService } from './restaurant.service';
import { Roles } from 'src/auth/decorator/role.decorator';
import {
  CreateRestaurantDto,
  RestaurantFilterType,
  RestaurantPaginationResponseType,
  UpdateRestaurantDto,
} from './dto/restaurant.dto';
import { Restaurant } from '@prisma/client';
import { Request } from 'express';

@Controller('restaurant')
export class RestaurantController {
  constructor(private readonly restaurantService: RestaurantService) {}
  @Post()
  @Roles('RES')
  create(
    @Body() body: CreateRestaurantDto,
    @Req() req: Request,
  ): Promise<Restaurant> {
    return this.restaurantService.create(body, req);
  }
  @Get()
  @Roles('AD')
  getAll(
    filter: RestaurantFilterType,
  ): Promise<RestaurantPaginationResponseType> {
    console.log('run api get all restaurant');
    return this.restaurantService.getAll(filter);
  }
  @Get('id')
  getDetail(@Param('id', ParseIntPipe) id: number): Promise<Restaurant> {
    console.log('run api get detail restaurant ', id);
    return this.restaurantService.getDetail(id);
  }
  @Put('id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() body: UpdateRestaurantDto,
  ): Promise<Restaurant> {
    console.log('run api update restaurant ', id);
    return this.restaurantService.update(id, body);
  }
  @Delete('id')
  delete(@Param('id', ParseIntPipe) id: number): Promise<Restaurant> {
    console.log('run api delete ', id);
    return this.restaurantService.delete(id);
  }
}
