import {
  HttpException,
  HttpStatus,
  Injectable,
  Req,
  UnauthorizedException,
} from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import {
  CreateRestaurantDto,
  RestaurantFilterType,
  RestaurantPaginationResponseType,
  UpdateRestaurantDto,
} from './dto/restaurant.dto';
import { Restaurant } from '@prisma/client';
import { Request } from 'express';
import { extractTokenFromHeader } from 'helpers/extractTokenFromHeader';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
@Injectable()
export class RestaurantService {
  constructor(
    private prismaService: PrismaService,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}
  async create(
    body: CreateRestaurantDto,
    @Req() req: Request,
  ): Promise<Restaurant> {
    const token = await extractTokenFromHeader(req);
    if (!token) {
      throw new UnauthorizedException();
    }
    const payload = await this.jwtService.verifyAsync(token, {
      secret: this.configService.get<string>('ACCESS_TOKEN_KEY'),
    });
    const addressId: number = Number(body.addressId);
    if (payload.roles !== 'RES') {
      throw new UnauthorizedException();
    }
    const address = await this.prismaService.address.findUnique({
      where: { id: addressId },
    });
    if (!address) {
      throw new HttpException(
        { message: 'address does not exists' },
        HttpStatus.BAD_REQUEST,
      );
    }
    const accountId: number = payload.id;

    const restaurant = await this.prismaService.restaurant.create({
      data: {
        addressId: addressId,
        addressDetail: body.addressDetail,
        accountId: accountId,
        phone: body.phone,
        name: body.name,
        detail: body.detail,
      },
    });
    return restaurant;
  }
  async getAll(
    filter: RestaurantFilterType,
  ): Promise<RestaurantPaginationResponseType> {
    const items_per_page: number = Number(filter.items_per_page) || 10;
    const page: number = filter.page || 1;
    const search: string = filter.search || '';
    const skip = page - 1 ? (page - 1) * items_per_page : 0;
    const restaurant = await this.prismaService.restaurant.findMany({
      take: items_per_page,
      skip: skip,
      where: {
        OR: [
          { name: { contains: search } },
          { addressDetail: { contains: search } },
          { phone: { contains: search } },
        ],
      },
      orderBy: { createdAt: 'desc' },
    });
    const total = await this.prismaService.restaurant.count({
      take: items_per_page,
      skip: skip,
      where: {
        OR: [
          { name: { contains: search } },
          { addressDetail: { contains: search } },
          { phone: { contains: search } },
        ],
      },
      orderBy: { createdAt: 'desc' },
    });
    return {
      data: restaurant,
      total,
      currentPage: page,
      itemsPerPage: items_per_page,
    };
  }
  async getDetail(id: number): Promise<Restaurant> {
    const restaurant = await this.prismaService.restaurant.findFirst({
      where: { id },
    });
    if (!restaurant) {
      throw new HttpException(
        { message: 'restaurant not found' },
        HttpStatus.NOT_FOUND,
      );
    }
    return restaurant;
  }
  async update(id: number, data: UpdateRestaurantDto): Promise<Restaurant> {
    const restaurant = await this.prismaService.restaurant.findFirst({
      where: { id },
    });
    if (!restaurant) {
      throw new HttpException(
        { message: 'restaurant not found' },
        HttpStatus.NOT_FOUND,
      );
    }
    if (data.addressId) {
      const addressId = data.addressId;
      const address = await this.prismaService.address.findFirst({
        where: { id: addressId },
      });
      if (!address) {
        throw new HttpException(
          { message: 'address does not exists' },
          HttpStatus.NOT_FOUND,
        );
      }
    }
    return await this.prismaService.restaurant.update({
      where: { id },
      data,
    });
  }
  async delete(id: number): Promise<Restaurant> {
    const restaurant = await this.prismaService.restaurant.findFirst({
      where: {
        id,
      },
    });
    if (!restaurant) {
      throw new HttpException(
        { message: 'restaurant not found' },
        HttpStatus.NOT_FOUND,
      );
    }
    const accountId: number = restaurant.accountId;
    const account = await this.prismaService.account.findFirst({
      where: { id: accountId },
    });
    if (!accountId) {
      throw new HttpException(
        { message: 'account not found' },
        HttpStatus.NOT_FOUND,
      );
    }
    const restaurantDel = await this.prismaService.restaurant.delete({
      where: { id },
    });
    await this.prismaService.account.delete({
      where: { id: accountId },
    });
    return restaurantDel;
  }
  
}
